package main

import (
	"fmt"
	"strconv"
	"strings"

	"org.kichnet/helpers"
)

type Command struct {
	instruction string
	param       int
}

type State struct {
	i                   int
	acc                 int
	visitedInstructions []int
	err                 string
}

func parseInstruction(cmdStr string) Command {
	parts := strings.Split(cmdStr, " ")
	param, err := strconv.Atoi(parts[1])
	if err != nil {
		panic(err)
	}
	return Command{parts[0], param}
}

func runProgram(program []Command) State {
	var runRec func(State) State
	runRec = func(state State) State {
		if state.i >= len(program) {
			fmt.Printf("Program terminated - we fixed it!")
			return state
		}
		if helpers.IsIntIn(state.visitedInstructions, state.i) {
			err := fmt.Sprintf("We found the infinite loop happening at instruction (%d) %v, with an accumulator value of %d\n", state.i, program[state.i], state.acc)
			return State{state.i, state.acc, state.visitedInstructions, err}
		}
		updatedVisitedList := append(state.visitedInstructions, state.i)
		currentCmd := program[state.i]
		switch currentCmd.instruction {
		case "nop":
			return runRec(State{state.i + 1, state.acc, updatedVisitedList, ""})
		case "acc":
			return runRec(State{state.i + 1, state.acc + currentCmd.param, updatedVisitedList, ""})
		case "jmp":
			return runRec(State{state.i + currentCmd.param, state.acc, updatedVisitedList, ""})
		default:
			return state
		}
	}
	return runRec(State{0, 0, []int{}, ""})
}

func main() {
	program := helpers.ReadLines("input.txt")
	commands := make([]Command, len(program))
	for i, cmdStr := range program {
		commands[i] = parseInstruction(cmdStr)
	}
	failingState := runProgram(commands)
	fmt.Printf("First failure %s", failingState.err)

	for i, cmd := range commands {
		if cmd.instruction == "nop" || cmd.instruction == "jmp" {
			modifiedProgram := make([]Command, len(commands))
			copy(modifiedProgram, commands)
			var changedInstruction string
			if cmd.instruction == "nop" {
				changedInstruction = "jmp"
			} else {
				changedInstruction = "nop"
			}
			modifiedProgram[i] = Command{changedInstruction, cmd.param}
			result := runProgram(modifiedProgram)
			if result.err == "" {
				fmt.Printf("We found a winner!  with a final accumulator value of %d", result.acc)
				return
			}
		}

	}
}
