package main

import (
	"fmt"
	"strings"

	"org.kichnet/helpers"
)

func parseGroupAnswers(group string) map[rune]int {
	var counts = make(map[rune]int)
	group = strings.ReplaceAll(strings.ReplaceAll(group, "\n", ""), " ", "")
	for _, q := range []rune(group) {
		count, exists := counts[q]
		if exists {
			counts[q] = count + 1
		} else {
			counts[q] = 1
		}
	}
	return counts
}

func countYesAnswers(counts map[rune]int, groupMembers int) (int, int) {
	anyCount := 0
	allCount := 0
	for r := range counts {
		anyCount++
		if counts[r] == groupMembers {
			allCount++
		}
	}
	return anyCount, allCount
}

func main() {
	groups := helpers.ReadInputAsNewlineSeparatedChunks("input.txt")
	anyCount := 0
	allCount := 0
	for _, group := range groups {
		counts := parseGroupAnswers(group)
		any, all := countYesAnswers(counts, len(strings.Split(group, "\n")))
		anyCount += any
		allCount += all
	}
	fmt.Printf("Found a total sum of %d ANY questions answered with yes across all groups\n", anyCount)
	fmt.Printf("Found a total sum of %d ALL questions answered with yes across all groups\n", allCount)

}
