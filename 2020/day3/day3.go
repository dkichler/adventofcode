package main

import (
	"fmt"

	"org.kichnet/helpers"
)

const Tree = rune('#')

type Slope struct {
	down  int
	right int
}

type Terrain struct {
	terrain [][]rune
	height  int
	width   int
}

func parseTerrain(path string) Terrain {
	terrainMap := helpers.ReadInputAsRuneArrays(path)
	return Terrain{terrainMap, len(terrainMap), len(terrainMap[0])}
}

func TreesOnPathWithSlope(terrain Terrain, slope Slope) int {
	trees := 0
	forward := 0
	for i := 0; i < terrain.height-slope.down; i += slope.down {
		forward += slope.right
		if i < terrain.height-slope.down && terrain.terrain[i+slope.down][(forward%terrain.width)] == Tree {
			trees++
		}
	}
	fmt.Printf("Made it to the bottom using slope %d down and %d right and only hit %d trees!\n", slope.down, slope.right, trees)
	return trees
}

func main() {
	terrain := parseTerrain("input.txt")

	product := TreesOnPathWithSlope(terrain, Slope{1, 3}) *
		TreesOnPathWithSlope(terrain, Slope{1, 1}) *
		TreesOnPathWithSlope(terrain, Slope{1, 5}) *
		TreesOnPathWithSlope(terrain, Slope{1, 7}) *
		TreesOnPathWithSlope(terrain, Slope{2, 1})

	fmt.Printf("Product of all the tree strikes together is %d", product)
}
