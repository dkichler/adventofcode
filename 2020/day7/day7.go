package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"org.kichnet/helpers"
)

type BagRule struct {
	bagColour   string
	allowedBags []string
	allowedNums map[string]int
}

func (br *BagRule) holdsBag(bagColour string) bool {
	for _, allowed := range br.allowedBags {
		if bagColour == allowed {
			return true
		}
	}
	return false
}

var re = regexp.MustCompile(`bags?\.?`)

func parseBagRule(bagRuleStr string) BagRule {
	parts := strings.Split(re.ReplaceAllString(bagRuleStr, ""), "contain")
	bagColour := strings.Trim(parts[0], " ")
	allowedBags := []string{}
	allowedNums := make(map[string]int)
	if strings.Contains(parts[1], "no other bags") {
		return BagRule{bagColour, allowedBags, allowedNums}
	}
	for _, allowedBagStr := range strings.Split(parts[1], ",") {
		allowedBagParts := strings.Split(strings.Trim(allowedBagStr, " "), " ")
		numAllowed, _ := strconv.Atoi(allowedBagParts[0])
		bagColour := strings.Join(allowedBagParts[1:], " ")
		allowedBags = append(allowedBags, bagColour)
		allowedNums[bagColour] = numAllowed
	}
	return BagRule{bagColour, allowedBags, allowedNums}
}

func countHowManyColoursCanEventuallyHold(bagPaths [][]string, inputBag string) []string {
	var uniqueBags []string
	for _, bagPath := range bagPaths {
		for _, bag := range bagPath {
			if bag != inputBag && !isIn(uniqueBags, bag) {
				uniqueBags = append(uniqueBags, bag)
			}
		}
	}
	return uniqueBags
}

func isIn(sl []string, v string) bool {
	for _, slv := range sl {
		if v == slv {
			return true
		}
	}
	return false
}

func isInArray(sl [][]string, v []string) bool {
	for _, slv := range sl {
		if arrayEqual(slv, v) {
			return true
		}
	}
	return false
}

func arrayEqual(a []string, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, av := range a {
		if av != b[i] {
			return false
		}
	}
	return true
}

func main() {
	input := "shiny gold"
	ruleLines := helpers.ReadLines("input.txt")
	rules := make(map[string]BagRule)
	ruleKeys := []string{}
	for _, rl := range ruleLines {
		rule := parseBagRule(rl)
		rules[rule.bagColour] = rule
		if rule.bagColour != input {
			ruleKeys = append(ruleKeys, rule.bagColour)
		}
	}

	var rec func([]string, [][]string) [][]string
	// start with set of all options = keys
	// pull out starting options
	// for each option, if the starting option will fit, add it to the stack and remove it from the options and recurse
	rec = func(path []string, paths [][]string) [][]string {
		newPaths := [][]string{path}
		for _, option := range ruleKeys {
			//if isIn(path, option) {
			//	continue
			//}
			lastBag := path[len(path)-1]
			bagRule := rules[option]
			if bagRule.holdsBag(lastBag) {
				newPaths = append(newPaths, rec(append(path, option), paths)...)
			}
		}
		return append(paths, newPaths...)
	}

	fmt.Printf("Number of rules %d\n", len(ruleKeys))
	fmt.Printf("Set: %v\n", rules[input].allowedBags)

	results := rec([]string{"shiny gold"}, [][]string{})
	// uniquePaths := [][]string{}
	for _, r := range results {
		fmt.Printf("Found path: %v\n", r)
		// if !isInArray(uniquePaths, r) {
		// 	uniquePaths = append(uniquePaths, r)
		// 	fmt.Printf("Unique path: %v\n", r)
		// }
	}
	uniqueBags := countHowManyColoursCanEventuallyHold(results, input)
	fmt.Printf("Got this many uniue bags: %v\n", uniqueBags)
	fmt.Printf("There seem to be %d uniquely coloured bags that could eventually contain a %s bag\n", len(uniqueBags), input)
}
