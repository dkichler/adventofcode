module aoc2020day7

go 1.15

replace org.kichnet/helpers => ../helpers

require (
	github.com/golang-collections/collections v0.0.0-20130729185459-604e922904d3
	org.kichnet/helpers v0.0.0-00010101000000-000000000000
)
