package main

import (
	"fmt"
	"strings"

	"org.kichnet/helpers"
)

type PasswordPolicy struct {
	LowerBound int
	UpperBound int
	Character  rune
	Password   string
}

func parsePasswordPolicy(policy string) PasswordPolicy {
	// uuuuuuuuugly!!
	parts := strings.Split(policy, ":")
	bounds := strings.Split(parts[0], " ")
	rangeNums := helpers.ToNums(strings.Split(bounds[0], "-"))
	return PasswordPolicy{rangeNums[0], rangeNums[1], []rune(bounds[1])[0], strings.Trim(parts[1], " ")}
}

func isOldPolicyValid(policy PasswordPolicy) bool {
	count := 0
	for _, c := range policy.Password {
		if c == policy.Character {
			count++
		}
	}
	return policy.LowerBound <= count && count <= policy.UpperBound
}

func isActualPolicyValid(policy PasswordPolicy) bool {
	pwdRunes := []rune(policy.Password)
	var isCharEqual = func(bound int) bool {
		return policy.Character == pwdRunes[bound-1]
	}
	return helpers.Xor(isCharEqual(policy.LowerBound), isCharEqual(policy.UpperBound))
}

func parsePolicies(lines []string) {
	valid := 0
	for _, p := range lines {
		policy := parsePasswordPolicy(p)
		if isActualPolicyValid(policy) {
			valid++
		}
	}
	fmt.Printf("Found %d valid policies", valid)
}

func main() {
	policyDefs := helpers.ReadLines("input.txt")
	parsePolicies(policyDefs)
}
