package main

import (
	"fmt"
	"strconv"
	"strings"

	"org.kichnet/helpers"
)

type Seat struct {
	row  int64
	seat int64
	id   int64
}

// Examples:
// BFFFBBFRRR: row 70, column 7, seat ID 567.
// FFFBBBFRRR: row 14, column 7, seat ID 119.
// BBFFBBFRLL: row 102, column 4, seat ID 820.

// F = 0
// B = 1
// L = 0
// R = 1
func toBinaryStr(partitionStr string) string {
	return strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(partitionStr, "F", "0"), "B", "1"), "L", "0"), "R", "1")
}

func parseSpacePartition(partition string) Seat {
	binary := toBinaryStr(partition)
	row, _ := strconv.ParseInt(binary[:7], 2, 64)
	seat, _ := strconv.ParseInt(binary[7:], 2, 64)
	return Seat{row, seat, seatId64(row, seat)}
}

func seatId(row int, seat int) int {
	return row*8 + seat
}

func seatId64(row int64, seat int64) int64 {
	return row*8 + seat
}
func main() {
	lines := helpers.ReadLines("input.txt")
	var highest int64 = 0
	var seatMap [128][8]rune
	for _, partitionStr := range lines {
		seat := parseSpacePartition(partitionStr)
		seatMap[seat.row][seat.seat] = 'X'
		if seat.id > highest {
			highest = seat.id
		}
	}
	fmt.Printf("Found the highest seat ID of %d\n", highest)
	// now look for the untaken seat
	foundStart := false
	for i := 0; i < 128; i++ {
		for j := 0; j < 8; j++ {
			seatTaken := seatMap[i][j] == 'X'
			fmt.Printf("Seat %d, %d is taken %t\n", i, j, seatTaken)
			if !foundStart {
				if seatTaken {
					foundStart = true
				}
			} else {
				if !seatTaken {
					fmt.Printf("I think we found my seat: Row %d Seat %d, with ID: %d\n", i, j, seatId(i, j))
					return
				}
			}
		}
	}

}
