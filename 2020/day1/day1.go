package main

import (
	"fmt"

	"org.kichnet/helpers"
)

func bruteForce(numbers []int) {
	var found = false
	for i, n1 := range numbers {
		if found {
			break
		}
		for j, n2 := range numbers {
			for k, n3 := range numbers {
				if found {
					break
				}
				if i == j || i == k || j == k {
					continue
				}
				if n1+n2+n3 == 2020 {
					fmt.Printf("Found numbers %d and %d and %d that sum to 2020\n", n1, n2, n3)
					fmt.Printf("Their product is: %d\n", n1*n2*n3)
					found = true
					break
				}
			}
		}
	}
}

func checkCombo(input []int) func([]int) bool {
	return func(combo []int) bool {
		sum := 0
		product := 1
		nums := make([]int, len(combo))
		for i, val := range combo {
			nums[i] = input[val]
			sum += nums[i]
			product *= nums[i]
		}
		if sum == 2020 {
			fmt.Printf("Found numbers %v that sum to 2020\n", nums)
			fmt.Printf("Their product is: %d\n", product)
			return true
		}
		return false
	}

}

func main() {
	ints := helpers.ReadInputAsInts("input.txt")
	bruteForce(ints)

	validator := checkCombo(ints)
	fmt.Printf("Got these matches: %v", helpers.ComboSearch(len(ints), 2, validator))
	fmt.Printf("Got these matches: %v", helpers.ComboSearch(len(ints), 3, validator))
}
