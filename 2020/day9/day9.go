package main

import (
	"fmt"

	"org.kichnet/helpers"
)

func checkCombo(input []int, targetSum int) func([]int) bool {
	return func(combo []int) bool {
		sum := 0
		for _, val := range combo {
			sum += input[val]
		}
		if sum == targetSum {
			return true
		}
		return false
	}
}

func main() {
	input := helpers.ReadInputAsInts("input.txt")
	windowSize := 25
	var offendingNumber int = 0
	for i := windowSize; i < len(input); i++ {
		window := input[i-windowSize : i]
		var test = checkCombo(window, input[i])
		combo := helpers.ComboSearch(len(window), 2, test)
		if combo == nil {
			fmt.Printf("Found the offending sequence number: %d at index %d\n", input[i], i)
			offendingNumber = input[i]
			break
		}
	}
	if offendingNumber == 0 {
		fmt.Printf("We finished the sequence without finding an offending number!\n")
		return
	}
	for winCheckSize := 2; winCheckSize < len(input); winCheckSize++ {
		for i := winCheckSize; i < len(input); i++ {
			window := input[i-winCheckSize : i]
			if helpers.Sum(window) == offendingNumber {
				fmt.Printf("We found a contiguous window of size %d (%d-%d) that sums to the offending number %d", winCheckSize, i-winCheckSize, i, offendingNumber)
				fmt.Printf("The sum of the min and max of that contiguous range is %d", helpers.Min(window)+helpers.Max(window))
			}
		}
	}
}
