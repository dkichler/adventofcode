package helpers

import (
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func ReadInput(path string) string {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	return strings.Trim(string(content), " \n")
}

func ReadLines(path string) []string {
	return strings.Split(ReadInput(path), "\n")
}

func ReadLinesAsParts(path string, transform func(string) []string) [][]string {

	return [][]string{}
}

func Xor(uno bool, dos bool) bool {
	return (uno || dos) && !(uno && dos)
}

func Sum(is []int) int {
	sum := 0
	for _, n := range is {
		sum += n
	}
	return sum
}

func Min(is []int) int {
	min := 0
	for _, n := range is {
		if min == 0 || n < min {
			min = n
		}
	}
	return min
}

func Max(is []int) int {
	max := 0
	for _, n := range is {
		if max == 0 || n > max {
			max = n
		}
	}
	return max
}

func ToNum(value string) int {
	num, err := strconv.Atoi(value)
	if err != nil {
		panic(err)
	}
	return num
}

func IsIntIn(sl []int, v int) bool {
	for _, slv := range sl {
		if v == slv {
			return true
		}
	}
	return false
}

func ReadInputAsInts(path string) []int {
	return ToNums(ReadLines(path))
}

func ReadInputAsNewlineSeparatedChunks(path string) []string {
	return strings.Split(ReadInput(path), "\n\n")
}

func ReadInputAsRuneArrays(path string) [][]rune {
	lines := ReadLines(path)
	rns := make([][]rune, len(lines))
	for i, str := range lines {
		rns[i] = []rune(str)
	}
	return rns
}

func ToNums(strs []string) []int {
	ints := make([]int, len(strs))
	for i, iStr := range strs {
		I, err := strconv.Atoi(iStr)
		if err != nil {
			panic(err)
		}
		ints[i] = I
	}
	return ints
}

// shameless ripoff:  https://stackoverflow.com/questions/127704/algorithm-to-return-all-combinations-of-k-elements-from-n
func ComboSearch(n int, k int, f func([]int) bool) [][]int {
	var comboSearchRec func([]int, int, int, [][]int) [][]int
	comboSearchRec = func(v []int, s int, j int, acc [][]int) [][]int {
		newCombos := [][]int{}
		if j == k {
			if f(v) {
				newCombos = append(newCombos, v)
			}
		} else {
			for i := s; i < n; i++ {
				newCombos = append(newCombos, comboSearchRec(append(v, i), i+1, j+1, [][]int{})...)
			}
		}
		return append(acc, newCombos...)
	}
	results := comboSearchRec([]int{}, 0, 0, [][]int{})
	if len(results) == 0 {
		return nil
	} else {
		return results
	}
}
