package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"org.kichnet/helpers"
)

// All fields:
// byr (Birth Year)
// iyr (Issue Year)
// eyr (Expiration Year)
// hgt (Height)
// hcl (Hair Color)
// ecl (Eye Color)
// pid (Passport ID)
// cid

var RequiredFields = [...]string{"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}

func ignoreCidField(passportData string) bool {
	for _, field := range RequiredFields {
		if !strings.Contains(passportData, field) {
			return false
		}
	}
	return true
}

type Field struct {
	name  string
	value string
}

func parsePassport(passportData string) []Field {
	fieldStrs := strings.Split(strings.ReplaceAll(passportData, "\n", " "), " ")
	fields := make([]Field, len(fieldStrs))
	for i, fs := range fieldStrs {
		fieldParts := strings.Split(fs, ":")
		fields[i] = Field{fieldParts[0], strings.Trim(fieldParts[1], " ")}
	}
	return fields
}

func passesCloserValidation(passportFields []Field) bool {
	for _, field := range passportFields {
		if !isFieldValid(field) {
			return false
		}
	}
	return true
}

// valids that all required fields are present AND of the correct form
func moreValidation(passportData []string) int {
	valid := 0
	for _, passportStr := range passportData {
		passportFields := parsePassport(passportStr)
		if ignoreCidField(passportStr) && passesCloserValidation(passportFields) {
			valid++
		}
	}
	return valid
}

func toNum(value string) int {
	num, err := strconv.Atoi(value)
	if err != nil {
		panic(err)
	}
	return num
}

func fourDigits(value string) bool {
	return len(value) == 4
}

var AllowedEyeColours = [...]string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}

// more stringent field validation
func isFieldValid(field Field) bool {
	result := false
	switch fieldType := field.name; fieldType {
	case "byr":
		birthYear := toNum(field.value)
		result = fourDigits(field.value) && birthYear >= 1920 && birthYear <= 2002
	case "iyr":
		issueYear := toNum(field.value)
		result = fourDigits(field.value) && issueYear >= 2010 && issueYear <= 2020
	case "eyr":
		expYear := toNum(field.value)
		result = fourDigits(field.value) && expYear >= 2020 && expYear <= 2030
	case "hgt":
		if strings.Contains(field.value, "in") {
			height := toNum(strings.ReplaceAll(field.value, "in", ""))
			result = height >= 59 && height <= 76
		} else if strings.Contains(field.value, "cm") {
			height := toNum(strings.ReplaceAll(field.value, "cm", ""))
			result = height >= 150 && height <= 193
		}
	case "hcl":
		match, _ := regexp.MatchString("^#[0-9a-f]{6}$", field.value)
		result = match
	case "ecl":
		for _, eyeColour := range AllowedEyeColours {
			if eyeColour == field.value {
				result = true
				break
			}
		}
	case "pid":
		match, _ := regexp.MatchString("^[0-9]{9}$", field.value)
		result = match
	case "cid":
		result = true
	default:
	}
	if !result {
		fmt.Printf("Rejecting field %s : %s\n", field.name, field.value)
	}
	return result
}

func isPassportValid(passportData string) bool {
	return ignoreCidField(passportData)
}

// checks only that all required fields are present (ignoring cid field)
// TODO factor out validation check into higher order function
func basicCheck(passports []string) int {
	validPassports := 0
	for _, passport := range passports {
		if isPassportValid(passport) {
			validPassports++
		}
	}
	return validPassports
}

func main() {
	input := helpers.ReadInput("input.txt")
	passports := strings.Split(input, "\n\n")

	//validPassports := basicCheck(passports)
	validPassports := moreValidation(passports)

	fmt.Printf("Found %d valid passports out of %d", validPassports, len(passports))
}
