#!/usr/bin/python

import sys
import string

#sys.setrecursionlimit(100000)
# we want to parse into steps and dependencies
def processInput(steps):
    stepDeps = {}
    for step in steps:
        # yay for assumption on uniform input
        dep = step[5]
        stepName = step[36]
        if stepName not in stepDeps:
            stepDeps[stepName] = [dep]
        else:
            stepDeps[stepName].append(dep)
        if dep not in stepDeps:
            stepDeps[dep] = []
    return stepDeps

def findNextStep(incompleteSteps, finished):
    # step is ready if all of its deps are done.  or if none of it's deps are unfinished
    finishedSet = set(finished)
    # candidates steps sorted alpha
    candidates = map(lambda s: s[0], sorted(filter(lambda step: set(step[1]) <= finishedSet, incompleteSteps.items()), key = lambda c: c[0], reverse = True))
    return candidates

baselineStepTime = 60
def startStep(currentTime, step, workerNum):
    stepDuration = baselineStepTime + string.ascii_uppercase.index(step) + 1
    endTime = currentTime + stepDuration
    workerSchedule[workerNum] = { 'step': step, 'endTime': endTime }

def completeAnySteps(currentTime, finishedSteps):
    finishedJobs = sorted(filter(lambda ws: currentTime >= ws[1]['endTime'], workerSchedule.items()), key = lambda ws: ws[1]['step'])
    for job in finishedJobs:
        workerSchedule.pop(job[0])
        availableWorkers.append(job[0])
        finishedSteps.append(job[1]['step'])


availableWorkers = [ 1, 2, 3, 4, 5 ]
workerSchedule = { }
if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        stripped = filter(lambda i: len(i.strip()) != 0, input.read().split('\n'))
        incompleteSteps = processInput(stripped)
        # (A, [B,C,D])
        totalSteps = len(incompleteSteps.keys())
        # now lets do the steps in the right order
        finishedSteps = []
        currentTime = 0
        while len(finishedSteps) < totalSteps:
            # find which workers can be released
            completeAnySteps(currentTime, finishedSteps)
            nextSteps = findNextStep(incompleteSteps, finishedSteps)
            # choose next steps based on available workers record when the worker will be done in the table
            while len(availableWorkers) > 0 and len(nextSteps) > 0:
                nextStep = nextSteps.pop()
                startStep(currentTime, nextStep, availableWorkers.pop())
                incompleteSteps.pop(nextStep)
            currentTime = currentTime + 1
        print "Final steps: ", ''.join(finishedSteps)
        print "Working time: ", currentTime - 1 # trim the final increment 
