#!/usr/bin/python

import sys
import re
import string

sys.setrecursionlimit(100000)

def triggerReactions(sequence):
    reactionResult = []
    i = 0
    while i + 1 < len(sequence):
        current = sequence[i]
        next = sequence[i+1]
        if ((current.islower() and next.isupper() and current.upper() == next) or
            (current.isupper() and next.islower() and current.lower() == next)): # reaction
            i = i + 2
        else: # no reaction
            reactionResult.append(current)
            i = i + 1
    if i == len(sequence) - 1: # if there's one final char left
        reactionResult.append(sequence[i])
    return reactionResult

def keepReacting(polymerSequence):
    reactionResultSequence = triggerReactions(polymerSequence)
    return polymerSequence if len(polymerSequence) == len(reactionResultSequence) else keepReacting(reactionResultSequence)

if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        fullSequence = input.read().strip()
        fullReaction = keepReacting(fullSequence)
        print "Part 1, unadultered input final polymer length: ", len(fullReaction)
        print "Part 2"
        noAs = filter(lambda unit: unit not in 'aA', fullSequence)
        results = []
        for char in string.ascii_lowercase:
            newElem = ("Final polymer length after input stripped of {}/{}: ".format(char.upper(), char.lower()), len(keepReacting(filter(lambda unit: unit not in char.upper() + char.lower(), fullSequence))) )
            print "New polymer after removing ", char, newElem
            results.append(newElem)
        print "Best performing: ", sorted(results, key = lambda r: r[1])[0]
