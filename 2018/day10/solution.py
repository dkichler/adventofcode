#!/usr/bin/python

import sys
import re


pointPattern = re.compile('position=<(.*)> velocity=<(.*)>')
# examnple: position=<-3,  6> velocity=< 2, -1>
def processInput(inputs):
    points = []
    for input in inputs:
        pointParts = pointPattern.match(input)
        if pointParts is None:
            print "didnt match this: ", input
        xy = pointParts.group(1).split(',')
        v = pointParts.group(2).split(',')
        points.append({
            'x': int(xy[0]),
            'y': int(xy[1]),
            'vx': int(v[0]),
            'vy': int(v[1])
        })
    return points

def movePoints(points, seconds = 1):
    return [{
        'x': point['x'] + seconds * point['vx'],
        'y': point['y'] + seconds * point['vy'],
        'vx': point['vx'],
        'vy': point['vy']
     } for point in points ]

# direct neighbours, no crossies
def areNeighbours(point1, point2):
    results = abs(point1['x'] - point2['x']) + abs(point1['y'] - point2['y']) <= 1
    #print "Testing: ", point1, point2, " Result: ", results
    return abs(point1['x'] - point2['x']) + abs(point1['y'] - point2['y']) <= 1

def pointsWithNeighours(points):
    return [p1 for p1 in points if len([p2 for p2 in points if p1 != p2 and areNeighbours(p1, p2)]) > 0]

# word threshold
wordThreshold = .95
def isWord(points, second):
    # calculate how many have neighbours
    withNeighbours = pointsWithNeighours(points)
    if second % 60 == 0:
        print "neighbour percentage: ", float(len(withNeighbours)) / float(len(points))
    return len(withNeighbours) / len(points) > wordThreshold

def findCoordSpace(points):
    pointSet = set([(point['x'], point['y']) for point in points])
    xsorted = sorted(points, key = lambda p: p['x'])
    minx = xsorted[0]['x'] - 2
    maxx = xsorted[-1]['x'] + 3
    ysorted = sorted(points, key = lambda p: p['y'])
    miny = ysorted[0]['y'] - 2
    maxy = ysorted[-1]['y'] + 3
    return ((minx, miny), (maxx, maxy))

def printWord(points):
    ((minx, miny), (maxx, maxy)) = findCoordSpace(points)
    pointSet = set([(point['x'], point['y']) for point in points])
    space = [''.join(['#' if (x,y) in pointSet else '.' for x in range(minx, maxx)]) for y in range(miny, maxy)]
    for line in space:
        print line

def playGame(points):
    word = None
    second = 10000
    points = movePoints(points, second)
    highestRatio = 0
    while word == None:
        points = movePoints(points)
        second = second + 1
        # calculate how many have neighbours
        withNeighbours = pointsWithNeighours(points)
        neighbourRatio = float(len(withNeighbours)) / float(len(points))
        if (neighbourRatio > highestRatio):
            highestRatio = neighbourRatio
            highestRatioSecond = second
        if neighbourRatio >= wordThreshold:
            word = points
        if second % 500 == 0:
            ((minx, miny), (maxx, maxy)) = findCoordSpace(points)
            print "MAX: ", highestRatio, highestRatioSecond
            print "neighbour percentage: ", neighbourRatio
            print "Coord space: ", maxx - minx, maxy - miny
            if max(maxx - minx, maxy - miny) < 500:
                print "Smallish area: "
                printWord(points)
    print "Word found in second ", second
    printWord(word)



if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        stripped = [line.strip() for line in input.read().split('\n') if len(line.strip()) > 0]
        allPoints = processInput(stripped)
        playGame(allPoints)
