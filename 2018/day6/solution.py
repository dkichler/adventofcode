#!/usr/bin/python

import sys
import string

#sys.setrecursionlimit(100000)
# process input lines into coordinates: (x, y)
def processInput(input):
    split = map(lambda i: i.strip(), input.split(','))
    return (int(split[0]), int(split[1]))

def findManhattenDistance(point1, point2):
    return abs(point2[0] - point1[0]) + abs(point2[1] - point1[1])

def keepReacting(polymerSequence):
    reactionResultSequence = triggerReactions(polymerSequence)
    return polymerSequence if len(polymerSequence) == len(reactionResultSequence) else keepReacting(reactionResultSequence)

# closestPoints: (coord: point that is closest to coord, if exists, None otherwise)
def findArea(point, cps):
    return map(lambda coord: coord[0], filter(lambda coordPoint: coordPoint[1] == point, cps.items()))

def isInfinite(point):
    # point is infinite if it touches the coordinate space boundary
    return point[0] == minX or point[0] == maxX or point[1] == minY or point[1] == maxY

def isAreaInfinite(area):
    return len(filter(lambda p: isInfinite(p), area)) > 0

if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        stripped = filter(lambda i: len(i.strip()) != 0, input.read().split('\n'))
        allPoints = map(processInput, stripped)
        # choose coordinate space -
        sortedX = sorted(allPoints, key = lambda p: p[0])
        sortedY = sorted(allPoints, key = lambda p: p[1])

        minX = sortedX[0][0] - 1
        minY = sortedY[0][1] - 1
        maxX = sortedX[-1][0] + 1
        maxY = sortedY[-1][1] + 1

        coordStart = (minX, minY)
        coordEnd = (maxX, maxY)
        print "Coordinate space ", coordStart, " -> ", coordEnd
        allCoordCounts = {}
        closestPoints = {}
        # find manhatten distances from all points in the coordinate space to all points of interest
        for x in range(minX, maxX + 1):
            for y in range(minY, maxY + 1):
                allCoordCounts[(x,y)] = { p: findManhattenDistance((x,y), p) for p in allPoints }
                # now find if this coord is the shortest distance to one of the points, record if so
                byDistance = sorted(allCoordCounts[(x,y)].items(), key = lambda kv: kv[1])
                closestPoints[(x,y)] = byDistance[0][0] if byDistance[0][1] < byDistance[1][1] else None
        # now make areas for each point
        areas = {
            point: findArea(point, closestPoints) for point in allPoints
        }
        nonInfiniteAreas = {
            point[0]: point[1] for point in filter(lambda area: not isAreaInfinite(area[1]), areas.items())
        }
        sortedByArea = sorted(nonInfiniteAreas.items(), key = lambda nonInfiniteArea: -len(nonInfiniteArea[1]))
        print "size of largest non-infinite area: ", len(sortedByArea[0][1])

        safeZoneThreshold = 10000
        coordsInSafeRegion = filter(lambda coord: sum(map(lambda pointCount: pointCount[1], coord[1].items())) < safeZoneThreshold, allCoordCounts.items())
        print "area of safezone: ", len(coordsInSafeRegion)
