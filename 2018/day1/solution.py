#!/usr/bin/python

import sys


def parseFreq(freq):
    return int(freq[1:]) if '+' in freq else -int(freq[1:])
#
# Accepts an array of frequency shifts:
# +1
# +1
# -2
# returns final frequency after applying all shifts
#
def determineFinalFrequency(frequencyShifts):
    return sum(map(lambda f: parseFreq(f), frequencyShifts))

# reads input string, splitting by newline, trimming and filtering empties
def processInput(inputStr):
    return list(filter(lambda f: len(f) != 0, map(lambda f: f.strip(), inputStr.split('\n'))))

def findFirstFrequencyAlreadyVisited(frequencyShifts):
    visitedFreqs = {}
    currentFreq = 0
    freqIndex = 0
    while currentFreq not in visitedFreqs:
        visitedFreqs[currentFreq] = 0
        currentFreq = currentFreq + parseFreq(frequencyShifts[freqIndex])
        freqIndex = (freqIndex + 1) % len(frequencyShifts)
    return currentFreq

if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        freqShifts = processInput(input.read())
        print "Final frequency: ", determineFinalFrequency(freqShifts)
        print "First frequency visited twice: ", findFirstFrequencyAlreadyVisited(freqShifts)
