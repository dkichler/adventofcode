#!/usr/bin/python

import sys
import re

# reads input string, splitting by newline, trimming and filtering empties
def processInput(inputStr):
    # guard # to entries:
    # { 10: { naps: [{start, end}, ...]] }
    splitAndTrimmed = list(filter(lambda f: len(f) != 0, map(lambda f: f.strip(), inputStr.split('\n'))))
    splitAndTrimmed = sorted(splitAndTrimmed)
    shiftIndices = [ i for i, entry in enumerate(splitAndTrimmed) if 'Guard' in entry ]
    # split into shifts
    shifts = [ splitAndTrimmed[shiftIndices[i]:len(splitAndTrimmed) if i+1 >= len(shiftIndices) else shiftIndices[i+1]] for i in range(0,len(shiftIndices)) ]
    guardEntries = {}
    for shift in shifts:
        guardNum = re.search('.*Guard #(\d+).*', shift[0]).group(1)
        if guardNum not in guardEntries:
            guardEntries[guardNum] = {
                'naps': []
            }
        # process naps
        i = 1
        while i + 1 < len(shift):
            nap = {
                'start': int(shift[i][15:17]),
                'end': int(shift[i+1][15:17])
            }
            i = i + 2
            guardEntries[guardNum]['naps'].append(nap)
    return guardEntries


if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        # map entries to shifts and naps
        guardEntries = processInput(input.read())
        for guardEntry in guardEntries.items():
            entry = guardEntry[1]
            entry['totalMinutes'] = sum(map(lambda nap: nap['end'] - nap['start'], entry['naps']))
            # find nap counts for every minute for each guard
            napCount = {}
            for minute in range(0,59):
                napCount[minute] = len(filter(lambda nap: minute >= nap['start'] and minute < nap['end'], entry['naps']))
            entry['bestMinutes'] = sorted(napCount.items(), key = lambda nc: -nc[1])[:5]
        sortedGuardEntries = sorted(guardEntries.items(), key = lambda entry: -entry[1]['totalMinutes'])
        sleepiestGuard = sortedGuardEntries[0]
        print "Sleepiest guard (by total sleep time): ", sleepiestGuard
        print "Checksum for the sleepiest guard (ID * sleepiest minute): ", int(sleepiestGuard[0]) * sleepiestGuard[1]['bestMinutes'][0][0]

        sortedGuardEntries = sorted(guardEntries.items(), key = lambda entry: -entry[1]['bestMinutes'][0][1])
        sleepiestGuard = sortedGuardEntries[0]
        print "Sleepiest guard (by frequency sleeping in the same minute): ", sleepiestGuard
        print "Checksum for the sleepiest guard (ID * sleepiest minute): ", int(sleepiestGuard[0]) * sleepiestGuard[1]['bestMinutes'][0][0]
