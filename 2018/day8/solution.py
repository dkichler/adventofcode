#!/usr/bin/python

import sys

sys.setrecursionlimit(100000)


def readNextNumber(stream):
    nextSpace = stream.index(' ') if ' ' in stream else len(stream)
    return (stream[nextSpace+1:], int(stream[:nextSpace]))

def parseNode(nodeStream, metaccumulator):
    (nodeStream, childrenCount) = readNextNumber(nodeStream)
    (nodeStream, metaCount) = readNextNumber(nodeStream)
    children = []
    while len(children) < childrenCount:
        (nodeStream, child, metaccumulator) = parseNode(nodeStream, metaccumulator)
        children.append(child)
    meta = []
    while len(meta) < metaCount:
        (nodeStream, nextMeta) = readNextNumber(nodeStream)
        meta.append(nextMeta)
        metaccumulator = metaccumulator + nextMeta
    node = {
      'children': children,
      'meta': meta,
      'value': sum(meta) if len(children) == 0 else sum([children[i-1]['value'] if i-1 >= 0 and i-1 < len(children) else 0 for i in meta])
    }
    return (nodeStream, node, metaccumulator)

if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        stripped = input.read().strip()
        (_, rootNode, metaccumulator) = parseNode(stripped, 0)
        print "Sum of all metadata: ", metaccumulator
        print "Value of root node: ", rootNode['value']
