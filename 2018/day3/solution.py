#!/usr/bin/python

import sys

def mapToEntryObj(entryStr):
    entryArray = entryStr.split(' ')
    return {
        'id': entryArray[0].strip(),
        'x': int(entryArray[2].strip().split(',')[0]),
        'y': int(entryArray[2].strip()[:-1].split(',')[1]),
        'width': int(entryArray[3].strip().split('x')[0]),
        'height': int(entryArray[3].strip().split('x')[1])
    }

# reads input string, splitting by newline, trimming and filtering empties
def processInput(inputStr):
    splitAndTrimmed = list(filter(lambda f: len(f) != 0, map(lambda f: f.strip(), inputStr.split('\n'))))
    return map(mapToEntryObj, splitAndTrimmed)

# maps a claim from it's starting point and dimensions to a set of coordinates that it covers
def mapClaim(entry):
    entry['coords'] = [ (x, y) for x in range(entry['x'], entry['x'] + entry['width']) for y in range(entry['y'], entry['y'] + entry['height']) ]
    return entry

if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        # map claims to coordinate space
        allClaims = list(map(mapClaim, processInput(input.read())))
        # count coordinates represented across all claims
        claimSquareCount = {}
        for claim in allClaims:
            for coord in claim['coords']:
                claimSquareCount[coord] = claimSquareCount[coord] + 1 if coord in claimSquareCount else 1
        print "Total squares covered by two or more claims: ", len(filter(lambda count: count >= 2, claimSquareCount.values()))
        # find claim that contends with no other
        goldenClaim = filter(lambda claim: len(filter(lambda coord: claimSquareCount[coord] == 1, claim['coords'])) == len(claim['coords']), allClaims)[0]
        print "ID of the golden claim: ", goldenClaim['id']
