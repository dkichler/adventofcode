#!/usr/bin/python

import sys

# reads input string, splitting by newline, trimming and filtering empties
def processInput(inputStr):
    return list(filter(lambda f: len(f) != 0, map(lambda f: f.strip(), inputStr.split('\n'))))

# maps an input string to a map of letter counts.  ie { a: 3, b: 5, ... }
def countLetters(input):
    letterCounts = {}
    for c in input:
        letterCounts[c] = letterCounts[c] + 1 if c in letterCounts else 1
    return letterCounts

# can handle strings of different length
def findCommonLetters(word1, word2):
    maxlen = min(len(word1), len(word2))
    commonLetters = []
    for i in range(0, maxlen):
        if word1[i] == word2[i]: commonLetters.append(word1[i])
    return commonLetters

if __name__ == "__main__":
    with open('input.txt', 'r') as input:
        allInputs = processInput(input.read())
        allCounts = []
        for input in allInputs:
            allCounts.append(countLetters(input))
        haveTwoCount = sum(list(map(lambda counts: 1 if 2 in counts.values() else 0, allCounts)))
        haveThreeCount = sum(list(map(lambda counts: 1 if 3 in counts.values() else 0, allCounts)))
        print "two and three counts: ", haveTwoCount, haveThreeCount
        print "Final checksum: ", haveTwoCount * haveThreeCount

        # ok, there are obviously many more intelligent search algorithms, but I'll start with brute force
        for input in allInputs:
            matchingBoxes = filter(lambda input2: input != input2 and len(input) - len(findCommonLetters(input, input2)) == 1, allInputs)
            if len(matchingBoxes) > 0:
                print "Common letters of matching boxes: ", ''.join(findCommonLetters(input, matchingBoxes[0]))
                break
