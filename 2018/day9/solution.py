#!/usr/bin/python

import sys
import re

sys.setrecursionlimit(100000)

# 10 players; last marble is worth 1618 points: high score is 8317
gamePattern = re.compile('(\d+) players; last marble is worth (\d+) points(: high score is (\d+))?')
def processInput(inputs):
    games = []
    for input in inputs:
        gameParts = gamePattern.match(input)
        games.append({
            'players': int(gameParts.group(1)),
            'lastMarble': int(gameParts.group(2)),
            'highScore': None if gameParts.group(4) is None else int(gameParts.group(4))
        })
    return games

# takes cirlce, nextMarble, and currentMarbleIndex
# returns updated circle, updated marble index, and any score resulting in the placement
def playNextMarble(circle, nextMarble, currentMarble):
    if len(circle) == 0 and currentMarble is None:
        circle.append(nextMarble)
        return (circle, 0, 0)
    else:
        if (nextMarble % 23 == 0):
            score = nextMarble
            removeMarbleIndex = (currentMarble - 7) % len(circle)
            score = score + circle[removeMarbleIndex]
            del circle[removeMarbleIndex]
            return (circle, 0 if removeMarbleIndex == len(circle) else removeMarbleIndex, score)
        else:
            nextMarbleIndex = (currentMarble + 2) % len(circle)
            if nextMarbleIndex == 0:
                circle.append(nextMarble)
                return (circle, min(currentMarble + 2, len(circle) - 1), 0)
            else:
                circle.insert(nextMarbleIndex, nextMarble)
                return (circle, nextMarbleIndex, 0)


def playGame(numberOfPlayer, lastMarbleValue):
    circle = []
    players = range(0, numberOfPlayer)
    scores = { p: 0 for p in players }
    currentMarble = None
    currentPlayerIndex = 0 # 0 player placed the first marble in play 0
    marble = 0
    while marble < lastMarbleValue:
        currentPlayer = players[currentPlayerIndex]
        (circle, currentMarble, score) = playNextMarble(circle, marble, currentMarble)
        # print "Circle: ", circle
        # print "current marble: ", currentMarble
        # print "Current marble: ", circle[currentMarble]
        scores[currentPlayer] = scores[currentPlayer] + score
        marble = marble + 1
        currentPlayerIndex = (currentPlayerIndex + 1) % len(players)
    return scores


if __name__ == "__main__":
    with open('test-input.txt', 'r') as input:
        stripped = [line.strip() for line in input.read().split('\n') if len(line.strip()) > 0]
        allGames = processInput(stripped)
        print "Games to test: ", allGames
        currentMarble = 0
        for game in allGames:
            scores = playGame(game['players'], game['lastMarble'])
            scoresSorted = sorted(scores.items(), key = lambda s: -s[1])
            print "Scores for game up to {}: {}".format(game['lastMarble'], scoresSorted[0])
