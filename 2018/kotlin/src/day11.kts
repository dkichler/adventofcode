data class Cell(val x: Int, val y: Int)

val power: MutableMap<Cell, Int> = HashMap<Cell, Int>()

val gridSerialNum = 1718
/*
 *
    -Find the fuel cell's rack ID, which is its X coordinate plus 10.
    -Begin with a power level of the rack ID times the Y coordinate.
    -Increase the power level by the value of the grid serial number (your puzzle input).
    -Set the power level to itself multiplied by the rack ID.
    -Keep only the hundreds digit of the power level (so 12345 becomes 3; numbers with no hundreds digit become 0).
    -Subtract 5 from the pow level.

 */
fun computeCellPower(cell: Cell): Int {
  val rackId = cell.x + 10
  return (((((rackId * cell.y) + gridSerialNum) * rackId) % 1000) / 100) - 5
}

fun isValidGrid(startCell: Cell) = startCell.x + 2 < 300 || startCell.y + 2 < 300

fun computeGridPower(startCell: Cell): Int {
  if (!isValidGrid(startCell)) {
    return 0
  } else {
    var score = 0
    for (x in startCell.x..startCell.x + 2) {
      for (y in startCell.y..startCell.y + 2) {
        score += power.getOrDefault(Cell(x,y), 0)
      }
    }
    return score
  }
}

for (x in 1..300) {
  for (y in 1..300) {
    val cell = Cell(x,y)
    val powerScore = computeCellPower(cell)
    power.put(cell, powerScore)
  }
}

println("Grid num: $gridSerialNum")
println("Score for 122,79: ${power.get(Cell(122,79))}")
println("Score for 217,196: ${power.get(Cell(217,196))}")
println("Score for 101,153: ${power.get(Cell(101,153))}")
var topRatings = ArrayList<Triple<Cell, Int, Int>>()
var gridRatings = ArrayList<Pair<Cell,Int>>()
for (x in 1..300) {
  for (y in 1..300) {
    for (size in 1..300) {
      
    }
    val gridStart = Cell(x,y)
    gridRatings.add(Pair(gridStart, computeGridPower(gridStart)))
  }
}

gridRatings.sortBy { gr -> -gr.second }
val bestScore = gridRatings.first()
println("Best score: $bestScore")
